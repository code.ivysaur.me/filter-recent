# filter-recent

![](https://img.shields.io/badge/written%20in-PHP-blue)

A utility to filter recent items from GNOME applications.

GNOME applications keep a list of recently accessed URIs (documents, RDP sessions, ...) in the `~/.local/share/recently-used.xbel` file. Individual applications (e.g. `evince`) don't offer a level of control over this file, and if you are using GNOME applications on a non-GNOME desktop (e.g. Xfce) then other methods of removing entries are not necessarily easily available.

The `filter-recent` utility allows you to remove multiple entry ranges from this file, either on-demand, or automatically as part of e.g. `.bashrc` or `.profile`.

## Usage


```
Usage:
  filter-recent [OPTIONS]
  
Options:
  --xbel PATH           Custom path to the 'recently-used.xbel' file
  -i                    Replace file on disk
  --no-write            Do not replace file on disk (default)
  --stdout              Display output to stdout (use with --no-write)
  --contains SUBSTRING  Remove entries where the file path contains SUBSTRING
  --excise START END    Remove entries visited between START and END.
  --help                Display this message
  -v, --verbose         Verbose

Date formats:
  This version of 'filter-recent' uses PHP strtotime for parsing date bounds.
  Detailed information about the available date format support is available at
   the URL http://php.net/manual/en/function.strtotime.php .

```


## Changelog

2017-05-01 1.0
- Initial public release
- [⬇️ filter-recent-1.0.tar.xz](dist-archive/filter-recent-1.0.tar.xz) *(1.48 KiB)*

